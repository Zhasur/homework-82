const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);
    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const artist = await Artist.create(
        {
            title: 'Ceza',
            information: 'Bilgin Özçalkan (born 31 December 1976), also known by his stage name Ceza' +
                ' (pronounced je-ZAH, Turkish for \'punishment\'), is a Turkish rapper and songwriter.' +
                ' His 2015 song "Suspus" has been compared to Eminem\'s anti-Trump freestyle rap "Storm"'
        },
        {
            title: 'Taylor Swift',
            information: 'Taylor Alison Swift (born December 13, 1989) is an American singer-songwriter. ' +
                'As one of the world\'s leading contemporary recording artists, she is known for narrative songs about' +
                ' her personal life, which has received widespread media coverage.'
        }
    );

    const album = await Album.create(
        {
            title: 'Suspus (Album)',
            artist: artist[0]._id,
            releaseDate: 2015,
            image: 'ceza.jpg'
        },
        {
            title: 'Reputation (Album)',
            artist: artist[1]._id,
            releaseDate: 2019,
            image: 'Taylor_Swift-reputation.jpg'
        }
    );


    await Track.create(
        {
            title: 'Suspus',
            album: album[0]._id,
            duration: '4:21'
        },
        {
            title: 'Holocaust',
            album: album[0]._id,
            duration: '3:32'
        },
        {
            title: 'Look What You Made Me Do)',
            album: album[1]._id,
            duration: '4:15'
        },
        {
            title: '…Ready For It?',
            album: album[1]._id,
            duration: '3:30'
        }
    );

    await connection.close();

};

run().catch(error => {
    console.error('Something went wrong!', error);
});